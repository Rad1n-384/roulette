import java.util.Random;

public class RouletteWheel {
    private Random randomNum;
    private int number = 0;

    public RouletteWheel() {
        this.randomNum = new Random();
    }

    public void spin() {
        this.number = randomNum.nextInt(37);
    }

    public int getValue() {
        return number;
    }
}
