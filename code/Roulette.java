import java.util.Scanner;

public class Roulette 
{
    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        int playerChips = 1000;
        int playerPick;
        int playerBet;
        int result;
        int winnings;
        boolean x = true;
        String answer;

        while (x && playerChips >= 1) 
        {
            System.out.println("You have $" + playerChips);
            System.out.println("How much would you like to bet ?");
            playerBet = scan.nextInt();
            if (playerBet <= playerChips) 
            {
                x = false;
                playerChips = playerChips - playerBet;
                System.out.println("What number would you like to bet on?");
                playerPick = scan.nextInt();
                scan.nextLine();
                wheel.spin();
                result = wheel.getValue();

                System.out.println("The ball landed on " + result);

                if (playerPick == result) 
                {
                    winnings = (playerBet * 35 + playerBet);
                    System.out.println("Congratulations! You win $ "+ winnings);
                    playerChips = playerChips + winnings;
                } 
                else 
                {
                    System.out.println("You lost : (");
                }
                if (playerChips == 0)
                {
                    System.out.println("You have lost $" + (playerChips - 1000));
                    break;
                }
                System.out.println("Would you like to play another round ? y/n");
                answer = scan.nextLine();
                if (answer.equals("y") || answer.equals("yes")) 
                {
                    x = true;
                } 
                else if (answer.equals("n") || answer.equals("no")) 
                {
                    if (playerChips >= 1000) 
                    {
                        System.out.println("You have won $" + (playerChips - 1000));
                    } 
                    else if (playerChips < 1000) 
                    {
                        System.out.println("You have lost $" + (playerChips - 1000));
                    }
                }
            } 
            else 
            {
                System.out.println("Please input a valid bet!");
            }
        }
    }
}